import { GUI } from 'three/examples/jsm/libs/dat.gui.module';
import {
  AddGui,
  AddBasicMaterialGui,
  AddCameraGui,
  AddMeshGui,
} from 'types/gui.types';

export const addGui: AddGui = () => new GUI();

export const addSphereGui: AddMeshGui = (gui, sphere) => {
  const sphereFolder = gui.addFolder('Sphere');
  sphereFolder.add(sphere.position, 'x', -5, 5, 0.01);
  sphereFolder.add(sphere.position, 'y', -5, 5, 0.01);
  sphereFolder.add(sphere.position, 'z', -5, 5, 0.01);
  return gui;
};

export const addCameraGui: AddCameraGui = (gui, camera) => {
  const cameraFolder = gui.addFolder('Camera');
  cameraFolder.add(camera.position, 'z', 0, 10, 0.01);
  return gui;
};

export const addBasicMaterialGui: AddBasicMaterialGui = (gui, basicMaterial) => {
  const data = {
    color: basicMaterial.color.getHex(),
  };
  const basicMaterialFolder = gui.addFolder('BasicMaterial');
  basicMaterialFolder
    .addColor(data, 'color')
    .onChange(() => { basicMaterial.color.setHex(Number(data.color.toString().replace('#', '0x'))); });
  basicMaterialFolder.add(basicMaterial, 'wireframe');
  return gui;
};
