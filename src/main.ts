import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import Stats from 'three/examples/jsm/libs/stats.module';
import { addGui, addBasicMaterialGui, addCameraGui, addSphereGui } from './utils/gui';

const scene: THREE.Scene = new THREE.Scene();
const axesHelper = new THREE.AxesHelper(5);

const camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

const renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const controls = new OrbitControls(camera, renderer.domElement);

const sphereGeometry: THREE.IcosahedronBufferGeometry = new THREE.IcosahedronBufferGeometry(1, 7);
const material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({ color: 0x87ff });

const sphere: THREE.Mesh = new THREE.Mesh(sphereGeometry, material);
scene.add(sphere);

camera.position.z = 2;

window.addEventListener('resize', onWindowResize, false);
function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
	render();
}

const stats = Stats();
document.body.appendChild(stats.dom);

scene.add(axesHelper);

const gui = addGui();
addSphereGui(gui, sphere);
addCameraGui(gui, camera);
addBasicMaterialGui(gui, material);

const animate = function () {
	requestAnimationFrame(animate);
	controls.update();
	render();
	stats.update();
};

function render() {
  renderer.render(scene, camera);
}
animate();
