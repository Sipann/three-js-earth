import * as THREE from 'three';
import { GUI } from 'three/examples/jsm/libs/dat.gui.module';

export type AddGui = () => GUI;

export type AddMeshGui = (
  gui: GUI,
  sphere: THREE.Mesh,
) => GUI;

export type AddCameraGui = (
  gui: GUI,
  camera: THREE.PerspectiveCamera,
) => GUI;

export type AddBasicMaterialGui = (
  gui: GUI,
  basicMaterial: THREE.MeshBasicMaterial,
) => GUI;